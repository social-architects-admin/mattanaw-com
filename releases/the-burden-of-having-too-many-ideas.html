<pre><!--1--><!--
/* Markdown Template for Mattanaw.com Article Pages
/* Properties travel with the page

/* Template
&&templateType="article"

/* Doc Properties
&&htmlPageTitle="the-burden-of-having-too-many-ideas.html"
&&DocTitle="The Burden of Having Too Many Ideas"
&&DocSubtitle="An Aside in the Series on Managing Procrastination"
&&DocAuthor="Christopher Matthew Cavanaugh"
&&DocDescription="MEGA DESCRIPTION"
&&DocImage="images/fireworks.jpg"
&&ProfileImage="images/profile/rv-mattanaw-warmind.png"

/* Open Graph and Facebook Properties
&&PubDate="January 22<sup>nd</sup> 2017"
&&EditDate="Original Edition"
&&OgSourceUrl="http://www.mattanaw.com/the-burden-of-having-too-many-ideas.html"
&&OgType="article"
&&OgImage="http://www.mattanaw.com/images/fireworks.jpg"
&&OgVideoType=""
&&OgVideo=""
&&OgVideoWidth=""
&&OgVideoHeight=""
&&OgTitle="The Burden of Having Too Many Ideas"
&&OgSiteName="MATTANAW.COM"
&&OgDescription="An Aside in the Series on Managing Procrastination"


/* Twitter Card Properties
&&twitterCardType="summary"
&&twitterCardSite="@mattanaw"
&&twitterCardTitle="The Burden of Having Too Many Ideas"
&&twitterCardDescription="An Aside in the Series on Managing Procrastination"
&&twitterCardImage="http://www.mattanaw.com/images/fireworks.jpg"



--><!--1--></pre>
<pre><!--2--></pre>
<img width="600" class="ui huge center floated image transition visible" data-src="images/fireworks.jpg" src="images/fireworks.jpg">
<p>Wouldn't it be great to have an endless supply of incredible ideas? Ask anyone and they will say &quot;Yes! That would be amazing!&quot; But what would it be like to be extremely creative? One has to be a different kind of person to continually have ideas. After all, it is not the norm, so surely there are downsides.</p>
<p>It turns out it can be quite a handicap.</p>
<p>Why?— Have you ever seen a person in public frantically recording ideas in a notebook, oblivious to people, time and surroundings? How about someone who draws and sketches incessantly for fear of losing a vision? Imagine a person who does this everywhere—at work, at the gym, at school, at home... — <em>everywhere</em>.</p>
<p>I was that person for a long time. Sometimes I am still that person. Only now I know when it is allowable or necessary.</p>
<p>The truth is that having an abundance of ideas can have heavy consequences. My close family and friends could probably find examples when my writing behavior was inexplicable or excessive. The assumption that frequent ideation is a gift conceals the reality that for many it must be a burden. Creativity can lead to dysfunction. I know it firsthand.</p>
<p>New ideas have special power in our conscious experience. When we have them they are hard to shake off—they capture our interest and demand our attention. We can become absorbed in daydreams at the wrong time and place, when attention desperately needs to be applied to other tasks. Inability to effectively manage ideas can lead to unusual activities; some beneficial, some not. People who are abundant appear manic, even if that is not the correct interpretation (although sometimes it is), and are liable to face social disapproval. New ideas can be a chaotic synthesis of disparate information, making them hard to communicate to others. The desire to be understood leads to obsessively analyzing and making sense of them in formats others can understand, like art and writing. Depending on how hyper-creativity manifests, it can be a real impediment.</p>
<p>I'm lucky that I appear relatively normal when it happens to me (I think). I just look incredibly absorbed, but I do not receive negative reactions. Although in coffee houses, I have looked up after an hour in a trance to see people staring, wondering what I was doing. I get interest sometimes, but rarely negativity. I doubt it is the same experience for everyone; some must be less fortunate. When I &quot;let-loose&quot; in private, and am completely alone, I would probably be a disturbing sight for others to behold. In a few demanding situations at work that required rapid brainstorming, I went into complete creative mode openly—but I find it creates a fear reaction. In other words merely being oneself completely can cause social isolation.</p>
<p>Here are some pains of the experience of superabundance:</p>
<ul>
<li><strong>Stress of idea loss.</strong> Excess ideas are forgotten if they are not written down or rehearsed. This is unpleasant, and the more ideas one has, the more this loss is experienced.</li>
<li><strong>Ideas pile up.</strong> If you have too many ideas and you try to record them, they will become difficult to manage and organize. After a short time you can have a 400 page book of ideas.</li>
<li><strong>Cannot record them.</strong> Ideas can come so quickly you cannot write them down fast enough, or communicate them with speech.</li>
<li><strong>Archive too large to use.</strong> Once recorded they become difficult to review, and eventually you just have a massive unwieldy log of chaotic notes.</li>
<li><strong>Non-completion.</strong> Ideas require resources and work to bring them to fruition. As the list of ideas grows, one realizes only a small fraction of them are feasible. I find I can only work on perhaps one of a thousand of my ideas, and they are usually ones I care less about. However, it is the work on these lesser ideas that prepare the way for the realization of larger ones.</li>
<li><strong>False feeling of procrastination.</strong> Since there are many ideas that one clings to, one quickly feels that there is not enough time to do everything. This leads to a feeling of immobility, hopelessness and self blame. But it is only due to overwhelming oneself.</li>
<li><strong>Actual Procrastination.</strong> <em>&quot;[There's] nothing like many creative ideas to help procrastinate a boring task ahead.&quot;</em> This was pointed out to me by a Facebook friend. There can be a close relationship between the readiness to entertain exciting ideas, and the avoidance of real work. (See the parent article on <a href="http://www.mattanaw.com/strategy-one-tips-and-tools-managing-procrastination.html">managing procrastination</a>).</li>
<li><strong>Choosing becomes a challenge.</strong> Your mind is clogged by possibilities when you attempt to prioritize. Distractions are also numerous (new ideas, and old ideas recalled), so it is hard to stick to anything chosen.</li>
<li><strong>Secrecy.</strong> One can be reluctant to share thoughts. For a while I did not talk about my thoughts, because of the value I gave them. Most ideas I did share were confirmed as valuable to other people, so I learned that there was some risk in sharing.</li>
<li><strong>Unknowability.</strong> After a while one becomes unable to communicate effectively because thoughts are dominated by interrelated ideas that cannot be communicated to others.</li>
<li><strong>Devising systems of idea tracking, and idea hoarding.</strong> One can spend days devising ways to organize ideas. I still have not completed my strategy for tracking, updating, and retrieving my thoughts (and I work in the field of software).</li>
</ul>
<img width="600" class="ui large left floated image transition visible" data-src="images/brainstorm.jpg" src="images/brainstorm.jpg">
<p>In my early 20s, I would carry a notebook and try to write every idea I had down. I found this extremely difficult, and I ended up interrupting my work frequently to scratch out streams of consciousness. In college I was chronically distracted by mental interruptions—I valued my own ideas I was having more than my assignments. All my required reading aroused more ideas, and prompted me to pursue additional personal reading. There were times when ideas came so quickly that I could not write them fast enough before they were lost. One cannot hold onto 7 or 8 ideas at a time and record them as new ones spring up. Some must be left behind.</p>
<p>Instead of learning to abandon surplus ideas, I began to experiment with new methods of recording them. I began learning shorthand and purchased a voice recorder. I considered using American sign language while signing and speaking into a video camera. Fortunately I abandoned that idea. The only method that was nearly fast enough was typing and voice recording simultaneously, but it was time consuming to combine the information, and this strategy cannot be used while driving a car.</p>
<p>The suggestion to carry a notebook to write down ideas is not for everyone—at least not without caveats! Some people need instructions about what not to do, and are dangers to themselves with a notepad!</p>
<p>Eventually I saw the futility and took more realistic actions. Below are some tips that may be helpful for anyone experiencing excess ideas. This is an extreme case, but it should help for other problems surrounding the experience of having new ideas as well:</p>
<ul>
<li><strong>You won't return to your notes.</strong> It is unlikely you will return to the ideas you record. You'll find your recent ideas more attractive and motivating than previous ones. This is a sad realization, but after you truly accept this fact, it will not cause you any more suffering. It frees you up for real action concerning other ideas, and you will have less experiences that cause you depression.</li>
<li><strong>Use mental techniques to remember instead.</strong> You add notes faster than you can review them or organize them. So be really brief or try to content yourself with just thinking of them. Start looking into mnemonics and memory strategies to quickly store them and reduce the chance of forgetting.</li>
<li><strong>Activity on one idea is better than passive ideation.</strong> Having too many ideas can lead to chronic daydreaming. You'll have to choose between motionless brainstorming or active work on projects. This implies giving up on potential ideas.</li>
<li><strong>Distracting ideas are just that—distractions.</strong> Having many ideas is great, but it is better to be able to control when you have them. Let them go when you're doing something worthy of your attention, like actually realizing one of your previous ideas!</li>
<li><strong>See the futility</strong> in hoarding and secrecy. Clinging to ideas when you have thousands of them is futile. Just share all your ideas and you will find people loving you for your novelties.</li>
<li><strong>Don't overprotect your creations.</strong> You'll come to wish someone else achieved whatever you didn't have time for. Come to know the things you really won't work on. Don't take them all too seriously—only some of them. When you're older you may prefer that someone stole or completed your ideas first, instead of having them not come into existence at all. It is a joy to get confirmation that your idea was really valuable in the world!</li>
<li><strong>Be brief.</strong> Write down hints that make you recall complete ideas, instead of long descriptions. Then you can record quickly and your ideas are more manageable.</li>
<li><strong>Losing ideas can be better.</strong> Realize that most of your ideas will be forgotten, but may eventually contribute to better ideas, through subconscious synthesis with new information. In other words, you'll have better ideas later that depend on having and losing these ideas first. Later ideas are typically better than previous ones because of maturity and increased knowledge! So it is good to let go.</li>
<li><strong>Be patient.</strong> The best ideas stick with you, and when you have a real chance to act (another important lesson that comes with maturity), you'll have no trouble prioritizing.</li>
</ul>
<p>It is possible to be too creative. It is possible to be confused about how to make use of one's creativity, and to be without guidance on how to manage. I hope the above points provide some relief. It is painful to feel golden ideas fade into nothingness, but it isn't as bad as you think. It is necessary. Creativity can be a source of potential, but it can also be potential destroying if it is not controlled. You must value action over daydreaming!</p>
<p>Remember—if you are highly creative, you do have a real gift. You will always have great options about things to do—you simply need to choose. You will never be short of exciting things to do, but don't lose out on the excitement by not committing to anything! Let go of some ideas so that you can live out the others!</p>
