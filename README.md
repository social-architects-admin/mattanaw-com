==========
TERMS AND CONDITIONS
========== 

Updated: Wed Oct 2 12:31:12 AKDT 2019

First use of this website includes opting-into this agreement.

No corporations or corporate agents, i.e. Corporations, LLCs, Partnerships, Non-profits, or any other organization that is not a single individual acting independently of any organization), or external consultant and contractors, acting on behalf of a coproration, with or without compensation, hereafter collectively called "CORPORATION", may make use of this website for any purpose.

Copies of any source code or content, or matching source code or content, existing in social media websites is also prohibited from use by any CORPORATION.

ORGANIZATIONS are not permitted to make comparisons between this website and content as it exists in any social media site, or make any other programmatic usage of content. This includes mere access and retrieval, but includes any and all data comparison and data processing also.

This website and/or source code may not be treated as a portfolio or example of the work of the author or anyone else for employment purposes, since the CORPORATION is not permitted to make any use of the content at all, including retrieval and viewing.

Viewing of content from an initial access is granted, but no information may be used in any manner from that initial viewing, and any cache, history of retrieval, stored copies, or anything else involving this first access must be destroyed by COPORATION.

If there is a special reason to make an exception, please contact the owner at

mattanaw@mattanaw.com

The only corporation excluded from this terms and conditions agreement is:

Social Architects and Economists International, LLC

Individual, non-CORPORATION use of this website and access to the domain/server implies agreement with the following terms and conditions.

The entire websites of: 

Mattanaw.com
ChristopherMatthewCavanaugh.com
ChristopherMatthewCavanaugh.org
CMCavanaugh.com

And any clones of the websites living in any repository, including but not limited to those at

bitbucket.org
github.com
etc...


Are copyrights © of Christopher Matthew Cavanaugh, 1998 - Present. 

They may not be stored, copied, distributed, printed, or sold without permission from the owner. 

Copies from the repositories may be cloned for local viewing. 

All source code and content is exclusively written, edited (when that happens), and maintained by Christopher Matthew Cavanaugh, with the exception of common UI libraries in use, such as semantic-ui, jquery, and cryptojs. The external libraries can be easily identified by reviewing the source.

The content may be quoted with no more than 500 characters, and shared via social media channels, so long as it is backlinked to the source and the author is properly credited. 

Quotes must include all errors and may not include "[sic]" or any other indicator that there is an error in the text that might require editing.

Minor edits were done by others in 2016 and very early 2017 only.

All Rights Reserved.

Contents of this website do not represent the opinions of the author's work relations, including organizations under his ownership, or organizations to which he invests, or organizations he gives special permission for usage.

Contents of this website do not always represent the opinions of the author. The author shares what the author would not formally commit to with finality. 

All consequences of reactions to opinions here rest with the reader, including discriminatory practices against first amendment rights.

https://www.senate.gov/civics/constitution_item/constitution.htm#amendments

All software associated with this site, with the exception of free third party libraries, are copyrights © of Christopher Matthew Cavanaugh and Social Architects and Economists International, LLC

For any questions, please contact the author at mattanaw@mattanaw.com 
